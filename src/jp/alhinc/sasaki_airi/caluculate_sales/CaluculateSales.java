package jp.alhinc.sasaki_airi.caluculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CaluculateSales{


	public static void main(String[] args) {

		Map<String, String> branchNames = new HashMap<String, String>();
		Map<String, Long> branchSales = new HashMap<String, Long>();

		//コマンドライン引数が渡されていない場合のエラー処理
		int nLen = args.length;
		if(nLen != 1) {
			System.out.println("予期せぬエラーが発生しました");
		}

		if ( !readfile(args[0],branchNames,branchSales,"branch.lst" ) ) {
			return;
		}

		//売上ファイル
		File[] files = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<File>();
		for(int i = 0; i < files.length ; i++) {
				String fileName = files[i].getName();
				if(fileName.matches("^[0-9]{8}.rcd$")) {
					rcdFiles.add(files[i]);
				}

		}

		Collections.sort(rcdFiles);

		//連番チェック
		for(int i = 0; i<rcdFiles.size() -1; i++) {

			File file1 = rcdFiles.get(i);
			File file2 = rcdFiles.get(i+1);

			String fileName1 = file1.getName();
			String fileName2 = file2.getName();

			String x = fileName1.substring(0,8);
			String y = fileName2.substring(0,8);

			int a = Integer.valueOf(x);
			int b = Integer.valueOf(y);

			if(b-a != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			try {
				br= new BufferedReader ( new FileReader(rcdFiles.get(i)));

				ArrayList<String> fileContents = new ArrayList<>();
				String line1;
				while(((line1 = br.readLine()) != null)) {
					 fileContents.add(line1);
				}

				String fileName = rcdFiles.get(i).getName();

				//行数チェック
				if(fileContents.size() !=2) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;

				}
				//支店定義ファイルに支店コードが書き込まれているかの確認 containskey
				if(!branchNames.containsKey(fileContents.get(0))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;

				}

				//支店コード
				String branchCode = fileContents.get(0);

				//売上ファイルの金額が数字かの確認
				if(!fileContents.get(1).matches("^[0-9]$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;

				}

				//String型からLong型
				long fileSale = Long.parseLong(fileContents.get(1));

				//売上金額の加算
				Long saleAmount = branchSales.get(branchCode) + fileSale;

				//10桁を超えたかの確認
				if(saleAmount >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(branchCode, saleAmount);

			} catch (IOException e) {
				 System.out.println("予期せぬエラーが発生しました");
				 return;

			} finally {
				if(br != null) {
					try {
						br.close();

					}catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!writefile(args[0],"branch.out",branchNames,branchSales)) {
			return;
		}
	}


	private static boolean readfile(String folderPath, Map<String, String>branchNames, Map<String, Long>branchSales, String fileName) {
		//支店定義ファイル読み込み処理
		BufferedReader br = null;
		try{
			File file = new File(folderPath, fileName);


			//支店定義ファイル存在チェック
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;

			}

			//支店定義ファイルの読み込み
			br = new BufferedReader(new FileReader(file));

			//読み込み処理の繰り返し（なくなるまで）
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//数字３桁との確認

				if((!items[0].matches("^[0-9]{3}$")) || (items.length != 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;

				}


				//ファイルから情報を引き題してMAPに入れる
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}


		} catch (IOException e) {
			 System.out.println("予期せぬエラーが発生しました");
			 return false;

		} finally {
			if(br != null) {
				try {
					br.close();


				}catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	private static boolean writefile(String folderPath, String fileName, Map<String, String>branchNames, Map<String, Long>branchSales) {
		//支店別ファイルファイル書き込み処理
		BufferedWriter bw = null;
		try{
			File file = new File(folderPath, fileName);
			bw = new BufferedWriter(new FileWriter(file));


			for(String key : branchNames.keySet()) {
				bw.write(key + "," + (branchNames.get(key)) + "," + branchSales.get(key));
				bw.newLine();

			}
		}catch (IOException e) {
			 System.out.println("予期せぬエラーが発生しました");
			 return false;

		} finally {
			if(bw != null) {
				try {
					bw.close();
			}

				catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}

